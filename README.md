# rubiks

## Getting Started
To get setup with this project, you will need to install the latest version of [node](https://nodejs.org/en/).

Then, clone this repo and navigate to it in the command line.

Once there, run the following

```bash
# Install npm packages
npm install

# Build the project
npm run build

# Run the code
npm start
```

## Run Tests
To run the tests, simply run

```bash
npm run test
```