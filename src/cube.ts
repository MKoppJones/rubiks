import { Color, Face, Matrix } from './typings/faces';
import { FaceMove } from './typings/move';
import { rotateSquare } from './matrix';

export default class Cube {
	state: Matrix<Color>;
	width: number = 3;

  // These define all the moves for a given 3x3 rubiks cube
  // NOTE: This could be made more consice, but I have opted for readability.
	moveDefinitions: FaceMove = {
		[Face.Front]: [
			{
				fromFace: Face.Up,
				fromIndices: [6, 7, 8],
				toFace: Face.Right,
				toIndices: [0, 3, 6],
			},
			{
				fromFace: Face.Right,
				fromIndices: [0, 3, 6],
				toFace: Face.Down,
				toIndices: [0, 1, 2],
			},
			{
				fromFace: Face.Down,
				fromIndices: [0, 1, 2],
				toFace: Face.Left,
				toIndices: [2, 5, 8],
			},
			{
				fromFace: Face.Left,
				fromIndices: [2, 5, 8],
				toFace: Face.Up,
				toIndices: [6, 7, 8],
			},
		],
		[Face.Right]: [
			{
				fromFace: Face.Front,
				fromIndices: [2, 5, 8],
				toFace: Face.Up,
				toIndices: [2, 5, 8],
			},
			{
				fromFace: Face.Up,
				fromIndices: [2, 5, 8],
				toFace: Face.Back,
				toIndices: [6, 3, 0],
			},
			{
				fromFace: Face.Back,
				fromIndices: [0, 3, 6],
				toFace: Face.Down,
				toIndices: [8, 5, 2],
			},
			{
				fromFace: Face.Down,
				fromIndices: [2, 5, 8],
				toFace: Face.Front,
				toIndices: [8, 5, 2],
			},
		],
		[Face.Up]: [
			{
				fromFace: Face.Front,
				fromIndices: [0, 1, 2],
				toFace: Face.Left,
				toIndices: [0, 1, 2],
			},
			{
				fromFace: Face.Left,
				fromIndices: [0, 1, 2],
				toFace: Face.Back,
				toIndices: [0, 1, 2],
			},
			{
				fromFace: Face.Back,
				fromIndices: [0, 1, 2],
				toFace: Face.Right,
				toIndices: [0, 1, 2],
			},
			{
				fromFace: Face.Right,
				fromIndices: [0, 1, 2],
				toFace: Face.Front,
				toIndices: [0, 1, 2],
			},
		],
		[Face.Back]: [
			{
				fromFace: Face.Right,
				fromIndices: [2, 5, 8],
				toFace: Face.Up,
				toIndices: [0, 1, 2],
			},
			{
				fromFace: Face.Up,
				fromIndices: [0, 1, 2],
				toFace: Face.Left,
				toIndices: [6, 3, 0],
			},
			{
				fromFace: Face.Left,
				fromIndices: [0, 3, 6],
				toFace: Face.Down,
				toIndices: [6, 7, 8],
			},
			{
				fromFace: Face.Down,
				fromIndices: [6, 7, 8],
				toFace: Face.Right,
				toIndices: [8, 5, 2],
			},
		],
		[Face.Left]: [
			{
				fromFace: Face.Up,
				fromIndices: [0, 3, 6],
				toFace: Face.Front,
				toIndices: [0, 3, 6],
			},
			{
				fromFace: Face.Front,
				fromIndices: [0, 3, 6],
				toFace: Face.Down,
				toIndices: [0, 3, 6],
			},
			{
				fromFace: Face.Down,
				fromIndices: [0, 3, 6],
				toFace: Face.Back,
				toIndices: [8, 5, 2],
			},
			{
				fromFace: Face.Back,
				fromIndices: [8, 5, 2],
				toFace: Face.Up,
				toIndices: [0, 3, 6],
			},
		],
		[Face.Down]: [
			{
				fromFace: Face.Front,
				fromIndices: [6, 7, 8],
				toFace: Face.Right,
				toIndices: [6, 7, 8],
			},
			{
				fromFace: Face.Right,
				fromIndices: [6, 7, 8],
				toFace: Face.Back,
				toIndices: [6, 7, 8],
			},
			{
				fromFace: Face.Back,
				fromIndices: [6, 7, 8],
				toFace: Face.Left,
				toIndices: [6, 7, 8],
			},
			{
				fromFace: Face.Left,
				fromIndices: [6, 7, 8],
				toFace: Face.Front,
				toIndices: [6, 7, 8],
			},
		],
	};

	constructor() {
    const size = this.width * this.width;
		this.state = [
			Array(size).fill(Color.Green), // Front
			Array(size).fill(Color.Red), // Right
			Array(size).fill(Color.White), // Up
			Array(size).fill(Color.Blue), // Back
			Array(size).fill(Color.Orange), // Left
			Array(size).fill(Color.Yellow), // Down
		];
	}

	rotate(face: Face, anticlockwise?: Boolean) {
		if (anticlockwise === undefined) {
			anticlockwise = false;
		}
    
    // Avoiding third-party libraries, this is a crude "deep-copy"
		const previousState = JSON.parse(JSON.stringify(this.state));

		this.state[face] = rotateSquare(
			this.state[face],
			this.width,
			anticlockwise,
		);

		const moves = this.moveDefinitions[face];

		for (const move of moves) {
			const prevFromFace = previousState[move.fromFace];
			const prevToFace = previousState[move.toFace];

			const fromFace = this.state[move.fromFace];
			const toFace = this.state[move.toFace];

			const { fromIndices, toIndices } = move;

			for (let index = 0; index < fromIndices.length; index += 1) {
				const from = fromIndices[index];
				const to = toIndices[index];
				if (anticlockwise) {
					fromFace[from] = prevToFace[to];
				} else {
					toFace[to] = prevFromFace[from];
				}
			}
		}
	}

	print() {
		let faceIndex = 0;
		for (const face of this.state) {
			const faceColors = face.map((f) => Color[f][0]);
			console.log(`\nFace: ${Face[faceIndex]}`);
			console.log(faceColors[0], faceColors[1], faceColors[2]);
			console.log(faceColors[3], faceColors[4], faceColors[5]);
			console.log(faceColors[6], faceColors[7], faceColors[8]);
			faceIndex += 1;
		}
	}
}
