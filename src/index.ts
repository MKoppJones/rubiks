import Cube from './cube';
import { Face } from './typings/faces';
import { Step } from './typings/move';

const cube = new Cube();

const steps: Array<Step> = [
	{ face: Face.Front, clockwise: true },
	{ face: Face.Right, clockwise: false },
	{ face: Face.Up, clockwise: true },
	{ face: Face.Back, clockwise: false },
	{ face: Face.Left, clockwise: true },
	{ face: Face.Down, clockwise: false },
];

for (const step of steps) {
	cube.rotate(step.face, !step.clockwise);
}

cube.print();
