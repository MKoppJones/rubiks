/**
 * Get the one dimensional index for a two dimensional coordinate
 * @param x 
 * @param y 
 * @param width The width of the 2D array
 * @returns The one dimensional position.
 */
export const indexSquare = (x: number, y: number, width: number): number => {
  if (width === 0) {
    return -1;
  }
  return x + y * width;
}

/**
 * Transpose a provided matrix of a given width.
 * @param matrix A one dimensional representation of a two dimensional array.
 * @param width The width of the square. Included to avoid square root.
 */
export const transposeSquare = (matrix: Array<any>, width: number) => {
	for (let x = 0; x < width; x += 1) {
		for (let y = 0; y < x; y += 1) {
			const xy = indexSquare(x, y, width);
			const yx = indexSquare(y, x, width);
			const temp = matrix[yx];
			matrix[yx] = matrix[xy];
			matrix[xy] = temp;
		}
	}
};

/**
 * Reverse each row of the matrix.
 * @param matrix A one dimensional representation of a two dimensional array.
 * @param width The width of the square. Included to avoid square root.
 * @returns A matrix with reversed rows.
 */
export const reverseRows = (matrix: Array<any>, width: number): Array<any> => {
	const newMatrix = [];
	for (let y = 0; y < width; y += 1) {
		const rowStart = indexSquare(0, y, width);
		const rowEnd = rowStart + width;
		for (let index = rowEnd - 1; index >= rowStart; index -= 1) {
			newMatrix.push(matrix[index]);
		}
	}
	return newMatrix;
};

/**
 * Rotates a given matrix by 90 degrees.
 * @param matrix A one dimensional representation of a two dimensional array.
 * @param width The width of the square. Included to avoid square root.
 * @param anticlockwise Should the matrix be rotated anticlockwise.
 * @returns A matrix rotated by 90 degrees.
 */
export const rotateSquare = (
	matrix: Array<any>,
	width: number,
	anticlockwise: Boolean,
): Array<any> => {
	if (anticlockwise) {
		const newMatrix = reverseRows(matrix, width);
		transposeSquare(newMatrix, width);
		return newMatrix;
	}
	transposeSquare(matrix, width);
	return reverseRows(matrix, width);
};
