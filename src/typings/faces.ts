export enum Face {
	Front,
	Right,
	Up,
	Back,
	Left,
	Down,
}

export enum Color {
	White,
	Orange,
	Green,
	Red,
	Blue,
	Yellow,
}

export type Matrix<T> = Array<Array<T>>