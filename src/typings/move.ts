import { Face } from './faces';

export type FaceMove = {
	[face in Face]: Array<Move>;
};

export type Move = {
	fromFace: Face;
	toFace: Face;
	fromIndices: Array<number>;
	toIndices: Array<number>;
};

export type Step = {
	face: Face;
	clockwise: boolean;
};
