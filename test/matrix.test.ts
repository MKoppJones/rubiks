import { indexSquare, reverseRows, rotateSquare, transposeSquare } from '../src/matrix';

describe('matrix.ts', () => {
	describe('indexSquare', () => {
		it('should return -1 for a 0 width position', () => {
			expect(indexSquare(0, 0, 0)).toBe(-1);
		});

		it('should return 5 for x=2 y=1 width=3', () => {
			expect(indexSquare(2, 1, 3)).toBe(5);
		});

		it('should return 1 for x=1 y=0 width=3', () => {
			expect(indexSquare(1, 0, 3)).toBe(1);
		});
	});

	describe('transposeSquare', () => {
		it('should transpose [ [1, 1], [2, 2] ] to [ [1, 2], [1, 2] ]', () => {
			const flatMatrix: Array<number> = [1, 1, 2, 2];
      const expectedMatrix: Array<number> = [1, 2, 1, 2];
      transposeSquare(flatMatrix, 2)
      expect(flatMatrix).toStrictEqual(expectedMatrix)
		});
	});

	describe('reverseRows', () => {
		it('should reverse rows [ [1, 2], [2, 1] ] to [ [2, 1], [2, 1] ]', () => {
			const flatMatrix: Array<number> = [1, 2, 2, 1];
			const expectedMatrix: Array<number> = [2, 1, 1, 2];
			expect(reverseRows(flatMatrix, 2)).toStrictEqual(expectedMatrix);
		});
	});

	describe('rotateSquare', () => {
		it('should rotate 90 degrees from [ [2, 3], [4, 5] ] to [ [4, 2], [5, 3] ]', () => {
			const flatMatrix: Array<number> = [2, 3, 4, 5];
			const expectedMatrix: Array<number> = [4, 2, 5, 3];
			expect(rotateSquare(flatMatrix, 2, false)).toStrictEqual(expectedMatrix);
		});

		it('should rotate -90 degrees from [ [2, 3], [4, 5] ] to [ [3, 5], [2, 4] ]', () => {
			const flatMatrix: Array<number> = [2, 3, 4, 5];
			const expectedMatrix: Array<number> = [3, 5, 2, 4];
			expect(rotateSquare(flatMatrix, 2, true)).toStrictEqual(expectedMatrix);
		});
	});
});
